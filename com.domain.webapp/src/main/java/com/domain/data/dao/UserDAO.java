package com.domain.data.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.domain.data.entities.User;

import io.carvill.data.dao.GenericDAO;

@Repository
public class UserDAO extends GenericDAO<User, Integer> {

	public UserDAO() {
		super(User.class);
	}

	public List<User> getAllUsers(){
        return this.select("User.getAllUsers");
    }
}

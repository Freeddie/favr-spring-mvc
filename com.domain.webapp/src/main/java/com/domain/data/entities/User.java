package com.domain.data.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import io.carvill.data.entity.BaseEntity;

@NamedQuery(name = "User.getAllUsers", query = "from User U")
@Table(name = "users")
@Entity
public class User extends BaseEntity<Integer> {

	private static final long serialVersionUID = 7920677614590455831L;

	@Column(name = "name", nullable = false, length = 30)
	private String name;

	@Column(name = "paternal", nullable = false, length = 20)
	private String paternal;

	@Column(name = "maternal", nullable = false, length = 20)
	private String maternal;

	@Column(name = "birthdate", nullable = false, length = 6)
	private Date birthdate;

	@Column(name = "age", nullable = false, length = 2)
	private Integer age;

	@Column(name = "gender", nullable = false, length = 1)
	private String gender;

	@Column(name = "address", nullable = false, length = 100)
	private String address;

	public void setName(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setPaternal(final String paternal) {
		this.paternal = paternal;
	}

	public String getPaternal() {
		return paternal;
	}

	public void setMaternal(final String maternal) {
		this.maternal = maternal;
	}

	public String getMaternal() {
		return maternal;
	}

	public void setBirthdate(final Date birthdate) {
		this.birthdate = birthdate;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setAge(final Integer age) {
		this.age = age;
	}

	public Integer getAge() {
		return age;
	}

	public void setGender(final String gender) {
		this.gender = gender;
	}

	public String getGender() {
		return gender;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", paternal=" + paternal + ", maternal=" + maternal + ", birthdate=" + birthdate
				+ ", age=" + age + ", geneder=" + gender + ", address=" + address + "]";
	}
	
	
}

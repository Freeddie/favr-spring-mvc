package com.domain.data.model;


import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class UserRequest implements Serializable {

	private static final long serialVersionUID = 7219474533296996376L;

	@NotBlank(message = "Name is required")
	@Length(max = 30, message = "Maximum length is 30") 
	private String name;
	
	@NotBlank(message = "Paternal is required")
	@Length(max = 20, message = "Maximum length is 20")
	private String paternal;
	
	@NotBlank(message = "Maternal is required")
	@Length(max = 20, message = "Maximum length is 20")
	private String maternal;
	
	@NotNull(message = "Birthdate is required")
	@Past
	private Date birthdate;
	
	@NotNull(message = "Age is Required")
	private Integer age;
	
	@NotBlank(message = "Gender is required")
	private String gender;
	
	@NotBlank(message = "Address is required")
	private String address;

	public void setName(final String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setPaternal(final String paternal){
		this.paternal = paternal;
	}
	
	public String getPaternal(){
		return paternal;
	}
	
	public void setMaternal(final String maternal){
		this.maternal = maternal;
	}
	
	public String getMaternal(){
		return maternal;
	}
	
	public void setBirthdate(final Date birthdate){
		this.birthdate = birthdate;
	}
	
	public Date getBirthdate(){
		return birthdate;
	}
	
	public void setAge(final Integer age){
		this.age = age;
	}
	
	public Integer getAge(){
		return age;
	}
	
	public void setGender(final String gender){
		this.gender = gender;
	}
	
	public String getGender(){
		return gender;
	}
	
	public void setAddress(final String address){
		this.address = address;
	}
	
	public String getAddress(){
		return address;
	}

	@Override
	public String toString() {
		return "UserRequest [name=" + name + ", paternal=" + paternal + ", maternal=" + maternal + ", birthdate="
				+ birthdate + ", age=" + age + ", gender=" + gender + ", address=" + address + "]";
	}
	
	
}

package com.domain.core;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.domain.core.exceptions.ConflictException;
import com.domain.data.dao.UserDAO;
import com.domain.data.entities.User;
import com.domain.data.model.UserRequest;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserOperations {

	@Autowired
	private UserDAO userDAO;
	
	protected void validations(final UserRequest request, final String validator) throws ConflictException{
		final int length = validator.length();
		for(int i = 0; i < length; i++){
    		char b = validator.charAt(i);
    		String c = Character.toString(b);
//    		System.out.println(c);
    		if(c.equals("ñ")){
//				System.out.println("Tiene una ñ");
				throw new ConflictException("No puede usar el caracter Ñ");
			}
    		for(int j = 0; j <= 9; j++){
//    			System.out.println(j);
    			String d = "" + j; 
    			if(c.equals(d)){
//    				System.out.println("Tiene número");
    				throw new ConflictException("No puedes usar números en el nombre y apellidos");
    			}
    		}
    	}
	}
	

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public User createUser(final UserRequest request) throws ConflictException{
		
    	String validator = request.getName();
    	validations(request, validator);
    	
    	validator = request.getPaternal();
    	validations(request, validator);
    	
    	validator = request.getMaternal();
    	validations(request, validator);
    	
    	if(request.getAge() < 18 | request.getAge() > 50){
//    		System.out.println("edad erronea");
    		throw new ConflictException("La edad es de 18 a 50 años");
    	}
    	
		final User user = new User();
        user.setName(request.getName());
        user.setPaternal(request.getPaternal());
        user.setMaternal(request.getMaternal());
        user.setBirthdate(request.getBirthdate());
        user.setAge(request.getAge());
        user.setGender(request.getGender());
        user.setAddress(request.getAddress());
        System.out.println(request);
        this.userDAO.persist(user);
        log.info("User #{} has been created", user.getId());
        return user;
	}


    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return this.userDAO.getAllUsers();
    }
}

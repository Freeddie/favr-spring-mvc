package com.domain.core.exceptions;

public class ConflictException extends Exception{

	private static final long serialVersionUID = -1336826324639431132L;
	
	 public ConflictException(String message, Throwable cause) {
	        super(message, cause);
	    }

	    public ConflictException(String message) {
	        super(message);
	    }
}

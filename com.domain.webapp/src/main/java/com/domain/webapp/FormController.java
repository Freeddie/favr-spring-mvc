package com.domain.webapp;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.domain.core.UserOperations;
import com.domain.core.exceptions.ConflictException;
import com.domain.data.model.UserRequest;

@Controller
public class FormController {

	@Autowired
	private UserOperations userOperations;
	
	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String index(final ModelMap model) {
		model.put("userRequest", new UserRequest());
		return "form";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String index(final ModelMap model, @ModelAttribute("userRequest") @Valid final UserRequest request,
			final BindingResult result) {
		if (result.hasErrors()) {
			model.put("userRequest", request);
			model.put("result", result);
			return "/form";
		}
		try {
			this.userOperations.createUser(request);
			return "redirect:/list";
		} catch (ConflictException e) {
			model.put("error", e.getMessage());
			return "/form";
		}
	}
}

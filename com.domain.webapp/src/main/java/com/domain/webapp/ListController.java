package com.domain.webapp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.domain.core.UserOperations;
import com.domain.data.entities.User;

@Controller
public class ListController {

	@Autowired
	private UserOperations userOperations;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String List(final ModelMap model){
		final List<User> users = this.userOperations.getAllUsers();
		model.put("user", users);
		return "list";
	}
}

<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.springframework.org/security/tags"   prefix="sec" %>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>ExamenWeb</title>

    <link rel="stylesheet" href="/static/css/font-awesome-4.5.0/css/font-awesome.min.css" media="screen">
    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/resources/css/webapp.css">

    <!--[if lt IE 9]>
	<script type="text/javascript" src="/static/js/html5shiv.min.js"></script>
	<script type="text/javascript" src="/static/js/respond.min.js"></script>
    <![endif]-->
    
	<script type="text/javascript" src="/static/js/jquery-1.12.1.min.js"></script>
	
    <decorator:head></decorator:head>
</head>
<body ng-app="AppModule">
	<div class="navbar navbar-inverse">
		<div class="container" ng-controller="MenuController as _menu">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" ng-click="_menu.toggleMenu()">
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand logo" href="/">
					Examen CPS
				</a>
			</div>
			<div id="navbar" class="{{_menu.visible ? '' : 'collapse navbar-collapse'}}">
				<ul class="nav navbar-nav">
					<li><a href="/form">Formulario</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container">
		<decorator:body></decorator:body>
	</div>
	
	<script type="text/javascript" src="/static/js/angular.min.js"></script>
	<script type="text/javascript" src="/static/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/resources/js/main.js"></script>
</body>
</html>
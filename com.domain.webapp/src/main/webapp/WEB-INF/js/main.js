angular.module('AppModule', [])
.controller('MenuController', function() {
	var _this = this;
	_this.visible = false;
	_this.toggleMenu = function() {
		_this.visible = !_this.visible;
	};

});
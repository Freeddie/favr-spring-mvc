<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<body>
	<div>
		<form:form action="/form" method="POST" modelAttribute="userRequest"
			cssClass="form-horizontal">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Formulario</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-12">
							<div class="form-group ${result.hasFieldErrors('name') ? 'has-error' : ''}">
								<label class="control-label col-sm-4" for="name">Nombre</label>
								<div class="col-sm-8">
									<form:input path="name" id="name" cssClass="form-control"
										placeholder="Nombre" />
									<form:errors path="name" delimiter=" - " cssClass="help-block"></form:errors>
								</div>
							</div>
							<div class="form-group ${result.hasFieldErrors('paternal') ? 'has-error' : ''}">
								<label class="control-label col-sm-4" for="paternal">Apellido
									Paterno</label>
								<div class="col-sm-8">
									<form:input path="paternal" id="patern" cssClass="form-control"
										placeholder="Apellido Paterno" />
									<form:errors path="paternal" delimiter=" - "
										cssClass="help-block"></form:errors>
								</div>
							</div>
							<div class="form-group ${result.hasFieldErrors('maternal') ? 'has-error' : ''}">
								<label class="control-label col-sm-4" for="maternal">Apellido
									Materno</label>
								<div class="col-sm-8">
									<form:input path="maternal" id="maternal"
										cssClass="form-control" placeholder="Apellido Materno" />
									<form:errors path="maternal" delimiter=" - "
										cssClass="help-block"></form:errors>
								</div>
							</div>
							<div class="form-group ${result.hasFieldErrors('birthdate') ? 'has-error' : ''}">
								<label class="control-label col-sm-4" for="birthdate">Fecha
									de Nacimiento</label>
								<div class="col-sm-8">
									<form:input path="birthdate" id="birthdate"
										cssClass="form-control" placeholder="aaaa/mm/dd" />
									<form:errors path="birthdate" delimiter=" - "
										cssClass="help-block"></form:errors>
								</div>
							</div>
							<div class="form-group ${result.hasFieldErrors('age') ? 'has-error' : ''}">
								<label class="control-label col-sm-4" for="age">Edad</label>
								<div class="col-sm-8">
									<form:input path="age" id="age" cssClass="form-control"
										placeholder="Edad" />
									<form:errors path="age" delimiter=" - " cssClass="help-block"></form:errors>
								</div>
							</div>
							<div class="form-group ${result.hasFieldErrors('gender') ? 'has-error' : ''}">
								<label class="control-label col-sm-4" for="gender">Genero</label>
								<div class="col-sm-8">
									<form:radiobutton path="gender" value="H" />
									Hombre
									<form:radiobutton path="gender" value="M" />
									Mujer
									<form:errors path="gender" delimiter=" - "
										cssClass="help-block"></form:errors>
								</div>
							</div>
							<div class="form-group ${result.hasFieldErrors('address') ? 'has-error' : ''}">
								<label class="control-label col-sm-4" for="address">Direcci�n</label>
								<div class="col-sm-8">
									<form:input path="address" id="address" cssClass="form-control"
										placeholder="Direcci�n" />
									<form:errors path="address" delimiter=" - "
										cssClass="help-block"></form:errors>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<div class="clearfix">
							<div class="pull-left">
								<p class="text-danger">${error}</p>
							</div>
							<div class="pull-right">
								<button type="submit" class="btn btn-info">
									<i class="fa fa-send"></i>Enviar
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form:form>
	</div>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h3>Lista de Usuarios</h3>
	<div class="container">
		<div class="clearfix">
			<div class="pull-right">
				<a href="/form" class="btn btn-success">Nuevo Usuario</a>
			</div>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Nombre</th>
					<th>Edad</th>
					<th>G�nero</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="users" begin="0" items="${user}">
					<tr>
						<td>${users.id}</td>
						<td>${users.name}</td>
						<td>${users.age}</td>
						<td>${users.gender}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>